$(document).ready(function() {
    $('.animate-a, .animate-b').viewportChecker({
        classToAdd: 'active',
        offset: 10
    });


    $('#pop1, #pop2').popup({
        transition: 'all 0.3s',
        closetransitionend: () => {
            $(".hide_on_submit").slideDown();
            $(".show_on_submit").slideUp();
            // $("#pop1")
            // form.reset();
            //
            // $("form").forEach(() => {
            //     console.log(item);
            // })
        }
    });


    $("#pop1").validate({
        rules: {
            name: "required",
            email: "required",
            question: "required"
        },
        errorPlacement: function(error, element){},
        submitHandler: function(){
            $(".hide_on_submit").slideUp("slow");
            $(".show_on_submit").slideDown("slow");
        }
    });


    $("#pop2").validate({
        rules: {
            name: "required",
            phone: "required"
        },
        errorPlacement: function(error, element){},
        submitHandler: function(){
            $(".hide_on_submit").slideUp("slow");
            $(".show_on_submit").slideDown("slow");
        }
    });


    $("#pop1, #pop2").submit( function () {
        let formdata = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "mail.php",
            data: formdata,
        });
        return false;
    });









    setTimeout(() => {
        $('#page-loading').fadeTo("slow", 0);
        setTimeout(() => {
            $('#page-loading').remove();
        }, 1000);
    }, 500);
});